import requests
import sys
from bs4 import BeautifulSoup

base_url = "https://eventyay.com/e/"
base_api_url = "https://api.eventyay.com/v1/events/"

total_error = 0
checked_id = []

# test = requests.get("https://blog.fossasia.org/gnome-asia-vietnam-wrap-up/")
# print(test.status_code)

def check_api(id):
    try:
        api_id = (base_api_url + id).strip()
        website_text = requests.get(api_id)
        if website_text.status_code != 200:
            print("")
            print("Status code: " + str(website_text.status_code))
            print(api_id)
            return False
        else:
            return True
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        print("")
        print(e)
        return False

def find_eventyay_link(url):
    global old_id
    global page_url
    global checked_id
    try:
        #send get request and use bs4
        if not ("http" in url):
            url = page_url + url
        req = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
        # print(url)
        # print(req.status_code)
        if req.status_code == 200: 
            html_source = req.text
            soup = BeautifulSoup(html_source, "html.parser")
            #case 1: link in redirect <meta> tag
            for link in soup.find_all("meta"):              #find the meta tag
                if link.get("http-equiv") == "refresh":     #check if there is refresh http-equiv
                    content = link.get("content")           #get the content attribute
                    redirect_link = content.split("url=")   #get the URL in the content
                    if len(redirect_link) == 2:
                        redirect_link = redirect_link[1]
                        if base_url in redirect_link:                        #check if the link is of eventyay
                            event_id = redirect_link.replace(base_url, "")  
                            event_id = event_id.split("/")                   #for the case there's parameter after the id
                            appeared = False
                            # print(checked_id)
                            for x in checked_id:
                                if event_id[0] == x[0]:
                                    appeared = True
                                    break
                            if not appeared:       
                                checked_id.append([event_id[0]])
                                return check_api(event_id[0])
            #case 2: link in <a> tag with href    
            for link in soup.find_all("a"):                             #find the <a> tag
                if link.get("href") != None:                            #check for attribute href
                    redirect_link = link.get("href")                    #get the link
                    if base_url in redirect_link:                       #check if the link is of eventyay
                        event_id = redirect_link.replace(base_url, "")  #get the event ID
                        event_id = event_id.split("/")                  #for the case there's parameter after the id
                        appeared = False
                        for x in checked_id:
                            if event_id[0] == x[0]:
                                appeared = True
                                break
                        if not appeared:       
                            checked_id.append([event_id[0]])
                            return check_api(event_id[0])
        elif req.status_code == 999 and ("linkedin" in url): #skip linkedin websites
            print("")
            print("linkedin website")
        #case 3: can not visit the link                               
        else:
            print("")
            print(url)
            print("status code not 200")
            print(req.status_code)
            return False
    except requests.exceptions.RequestException as e:  #If catch any error
        print("")
        print(url)
        print(e)
        return False

page_url = "http://events.fossasia.org/" #Page's url to check

website = requests.get(page_url) #Use requests to parse the html sources
website_text = website.text 
soup = BeautifulSoup(website_text, "html.parser") #Use BeautifulSoup

for link in soup.find_all("a"): #find all the tag <a> in the page
    url = link.get("href")
    if url != None: #check if it has attribute href
        url = url.strip()
        if base_url in link: 
            event_id = link.replace(base_url, "")  #get the event ID
            event_id = event_id.split("/")         #for the case there's parameter after the id                
            if (check_api(event_id[0]) == False):
                total_error += 1
        else:
            if url[:7] != "http://" and url[:8] != "https://": #case relative url
                if  (find_eventyay_link(page_url + url) == False):
                    print("Homepage: " + page_url + url)
                    total_error += 1
            else:
                #normal case
                if (find_eventyay_link(url) == False): 
                    print("Homepage: " + url)
                    total_error += 1

print(total_error)
sys.exit(total_error)