FROM python:3.8

COPY . .

RUN python -m pip install requests

RUN python -m pip install beautifulsoup4

CMD ["python", "parser.py"]